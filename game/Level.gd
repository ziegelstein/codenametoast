extends Node2D

var selected_blob
# Called when the node enters the scene tree for the first time.
func _ready():
	GameControl.add_background(self)
	GameControl.add_walls(self)
	GameControl.add_blobs(self, 8, 1)
	SoundControl.play_level_music()
	select_blob()


func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		var blobs = []
		for child in get_children():
			if "Blob" in child.get_name():
				blobs.append(child)
		select_blob()
		GameControl.color_click(blobs, self.selected_blob)
		
	elif Input.is_action_just_pressed("ui_alt"):
			for child in get_children():
				if "Blob" in child.get_name():
					if child.get_node("BlobBody").is_selected:
						child.get_node("BlobBody").set_shape(randi()%4)
						
func select_blob():
	for child in get_children():
		if "Blob" in child.get_name() && child.get_node("BlobBody").is_selected:
			selected_blob = child
extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	SoundControl.play_menu_music()
	GameControl.add_background($Field)
	GameControl.add_blobs($Field, 8)
	#GameControl.add_selected_blob($Field)
	GameControl.add_walls($Field)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass




func _on_1P_pressed():
	SoundControl.stop_music()
	get_tree().change_scene("res://Level.tscn")


func _on_Quit_pressed():
	get_tree().quit()

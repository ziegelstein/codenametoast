extends RigidBody2D

var radius = 30.0
export (int) var speed = 10
var randomizerX = randi() %2+1
var randomizerY = randi() %2+1
var velocity = self.get_linear_velocity()
const messages = ["Hallo", "My Name is Bob the Blob", "I was a bug, but now I'm the tutorial", "Click on your Mouse", "If you Collide with another Blob", "To Color it yellow", "Color all Blobs yellow", "Only ONE color must remain!", "Good Luck!", "...", "...", "...", "...", "...", "Well, here I am now", "Was supposed to be the main blob", "Got on the wrong path", "Now I'm stuck doing this", "...", "Tragic, isn't it?", "...", "...", "..."]
var message_iterator = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	$CollisionShape.shape = CircleShape2D.new()
	$CollisionShape.shape.set_radius(radius)
	$Label.get_font("normal_font").set("size", 25)
	$Timer.start(2)
	$Timer.connect("timeout", self, "_on_timer_timeout")
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _process(delta):
	randomizerX = randi() %2+1
	randomizerY = randi() %2+1

func _integrate_forces(state):
	# TODO Dirty Hack, make it more likely to keep current velocity
	if (randomizerX == 1):
		velocity.x += randi() % 10
		change_velocity(velocity)
	elif(randomizerX == 2):
		velocity.x -= randi() % 10
		change_velocity(velocity)
	if (randomizerY == 1):
		velocity.y += randi() % 10
		change_velocity(velocity)
	elif(randomizerY == 2):
		velocity.y -= randi() % 10
		change_velocity(velocity)
	pass
	
func change_velocity(velocity):
	velocity = velocity.normalized() * speed
	self.linear_velocity = velocity
	
func _on_timer_timeout():
	if message_iterator < messages.size()-1:
		message_iterator +=1
		$Label.text = messages[message_iterator]
	else:
		$Label.text = messages[0]
		message_iterator = 0
	$Timer.start()
	
func on_touch_screen_end():
	pass
	
func _draw():
	self.draw_circle(self.position, radius, Color.yellow)
extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var blob_scene = preload("res://Entities/Blob/Blob.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
#	var blob_node = blob_scene.instance()
#	var blob_node_2 = blob_scene.instance()
#	blob_node.position = Vector2(100,100)
#	blob_node.transform.scaled( Vector2(0.1,0.1) )
#	blob_node.get_node("BlobBody").linear_velocity.x = 200
#	blob_node_2.position = Vector2(100,100)
#	add_child(blob_node)
#	add_child(blob_node_2)
	var blob_node
	for i in range(10):
		blob_node = blob_scene.instance()
		blob_node.position = Vector2(100+i*10,100+i*5)
		#blob_node.transform.scaled( Vector2(0.1,0.1) ) -- Tut nix
		var blob_body = blob_node.get_node("BlobBody")
		#blob_body.get_node("spr").modulate = Color(0, 1, 0)
		blob_body.linear_velocity.x = 2000
		blob_body.set_shape(i%4)
		blob_body.set_color(GameControl.get_random_color())
		add_child(blob_node)
	GameControl.add_selected_blob(self)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

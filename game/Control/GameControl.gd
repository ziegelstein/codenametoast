extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var colors = [Color.red, Color.blue, Color.green]
export var player_color = Color.yellow

var blob_scene = preload("res://Entities/Blob/Blob.tscn")
var walls_scene = preload("res://Entities/Walls.tscn")
var background_scene = preload("res://UI/Background.tscn")

var same_shape = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func get_random_color():
	return colors[randi()%3]

func add_blobs(parent_node, number, player_blobs = 0):
	var blob_node
	for i in range(number+player_blobs):
		blob_node = blob_scene.instance()
		blob_node.position = Vector2(100+i*10,100+i*5)
		#blob_node.transform.scaled( Vector2(0.1,0.1) ) -- Tut nix
		var blob_body = blob_node.get_node("BlobBody")
		#blob_body.get_node("spr").modulate = Color(0, 1, 0)
		blob_body.linear_velocity.x = 2000
		blob_body.set_shape(i%4)
		if (i>=player_blobs):
			blob_body.set_color(GameControl.get_random_color())
		else:
			blob_body.set_color(Color.yellow)
			blob_body.toggle_select()
		
		parent_node.add_child(blob_node)
		
func add_selected_blob(parent_node):
	var blob_node = blob_scene.instance()
	blob_node.position = Vector2(100,100)
	var blob_body = blob_node.get_node("BlobBody")
	blob_body.set_shape(0)
	blob_body.set_color(Color.yellow)
	blob_body.toggle_select()
	parent_node.add_child(blob_node)

func add_walls(parent_node):
	parent_node.add_child(walls_scene.instance())
	
func check_win(blobs):
	var win = true
	for blob in blobs:
		var blobBody = blob.get_node("BlobBody")
		if (blobBody.has_method("get_color")):
			if (blobBody.get_color() != Color.yellow):
				win = false
				break
	if (win):
		SoundControl.stop_music()
		get_tree().change_scene("res://UI/MainMenu.tscn")
	#return win;
	
func color_click(blobs, selected_blob = null):
	var hacky_flag = true
	for blob in blobs:
		var blobBody = blob.get_node("BlobBody")
		if blobBody.is_color_changable:
			if same_shape && blobBody.get_shape() is selected_blob.get_node("BlobBody").get_shape():
				blobBody.set_color(player_color)
				if hacky_flag:
					blobBody.toggle_select()
					selected_blob.get_node("BlobBody").toggle_select()
					hacky_flag = false
				check_win(blobs)
			elif !same_shape:
				blobBody.set_color(player_color)
				if hacky_flag:
					blobBody.toggle_select()
					selected_blob.get_node("BlobBody").toggle_select()
					hacky_flag = false
				check_win(blobs)

func add_background(parent_node):
	var bg = background_scene.instance()
	parent_node.add_child(bg)
extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var player = AudioStreamPlayer.new()

var music = preload("res://assets/sound/Blubbern.ogg")
var menu_music = preload("res://assets/sound/spiele musik2.ogg")

# Called when the node enters the scene tree for the first time.
func _ready():
	player.volume_db = -10
	add_child(player)

func play_level_music():
	player.stream = music
	player.play(0)
	
func play_menu_music():
	player.stream = menu_music
	player.play(0)
	
func stop_music():
	player.stop()
extends RigidBody2D

export var Shapes = preload("res://Entities/Blob/Shapes.gd")

var _spr_circle = preload("res://assets/blob_round.png")
var _spr_cube = preload("res://assets/blob_cube.png")
var _spr_tri = preload("res://assets/blob_tri.png")
var _spr_hex = preload("res://assets/blob_hex.png")

var bells = [preload("res://assets/sound/Glocken 2.wav"), preload("res://assets/sound/Glocken 3.wav"), preload("res://assets/sound/Glocken 4.wav"), preload("res://assets/sound/Glocken 5.wav"), preload("res://assets/sound/Glocken 6.wav")]

var is_selected = false 
var is_color_changable = false

var click_time = 0.5 # seconds

export (int) var speed = 250
var randomizerX = randi() %3
var randomizerY = randi() %3
var velocity = self.get_linear_velocity()


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	randomizerX = randi() %2+1
	randomizerY = randi() %2+1
	pass
	

func _integrate_forces(state):
	# TODO Dirty Hack, make it more likely to keep current velocity
	if self.is_selected:
		velocity = self.get_linear_velocity()
		if (randomizerX == 1):
			velocity.x += randi() % 10
			change_velocity(velocity)
		elif(randomizerX == 2):
			velocity.x -= randi() % 10
			change_velocity(velocity)
		if (randomizerY == 1):
			velocity.y += randi() % 10
			change_velocity(velocity)
		elif(randomizerY == 2):
			velocity.y -= randi() % 10
			change_velocity(velocity)
	
func change_velocity(velocity):
	velocity = velocity.normalized() * speed
	self.linear_velocity = velocity

func set_shape(shape):
	if (typeof(shape) != TYPE_INT):
		print("Changed shape to non-shape!")
		get_tree().quit()
	
	var spr_width = _spr_tri.get_width()
	var spr_height = _spr_tri.get_height()
	match shape:
		Shapes.CIRCLE:
			$spr.set_texture(_spr_circle)
			$col.shape = CircleShape2D.new()
			$col.shape.radius = _spr_circle.get_width()/2
		Shapes.CUBE:
			$spr.set_texture(_spr_cube)
			$col.shape = RectangleShape2D.new()
			$col.shape.extents = Vector2(_spr_cube.get_width()/2, _spr_cube.get_height()/2)
		Shapes.HEX:
			$spr.set_texture(_spr_hex)
			$col.shape = ConvexPolygonShape2D.new()
			
			$col.shape.points = PoolVector2Array([Vector2(-spr_width/4,-spr_height/2), Vector2(spr_width/4,-spr_height/2), Vector2(spr_width/2,0), Vector2(spr_width/4,spr_height/2), Vector2(-spr_width/4, spr_height/2), Vector2(-spr_width/2,0)])
		Shapes.TRI:
			$spr.set_texture(_spr_tri)
			$col.shape = ConvexPolygonShape2D.new()
			$col.shape.points = PoolVector2Array([Vector2(-spr_width/2, spr_height/2), Vector2(spr_width/2, spr_height/2), Vector2(0,-spr_width/2)])

func set_color(color):
	$spr.modulate = color
	
func get_shape():
	return $col.shape

func toggle_select():
	if (is_selected):
		is_selected = false
		$part.emitting = false
		physics_material_override.friction = 1
	else:
		is_selected = true
		$part.emitting = true
		physics_material_override.friction = 0


func get_color():
	return $spr.modulate

func _on_BlobBody_body_shape_exited(body_id, body, body_shape, local_shape):
	pass


func _on_clickTime_timeout():
	self.is_color_changable = false

func _on_BlobBody_body_entered(body):
	if (body.has_method("set_color")):
		if (self.is_selected):
			body.is_color_changable = true
			var ClickTimer = body.get_node("ClickTimer")
			ClickTimer.start(click_time)
			$wop.play(0)
			ClickTimer.connect("timeout", body, "_on_clickTime_timeout")
			# dy.set_color(Color.yellow)
		elif (!body.is_selected):
			$audio.stream = bells[randi()%4]
			$audio.play(0)
			set_shape(randi()%4)
	pass
